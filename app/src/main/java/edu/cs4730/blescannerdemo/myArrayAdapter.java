package edu.cs4730.blescannerdemo;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;

import androidx.annotation.NonNull;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;

import java.util.List;

/**
 * Created by Seker on 3/2/2018.
 */

public class myArrayAdapter extends ArrayAdapter<ScanResult> {

    private List<ScanResult> list;
    private final Activity context;
    byte[] bytes=null;
    String logs="";

    public myArrayAdapter(@NonNull Activity context, @NonNull List<ScanResult> list) {
        super(context, R.layout.row, list);
        this.context = context;
        this.list = list;
    }


    public void setData(List<ScanResult> list) {
        this.list = list;
        notifyDataSetInvalidated();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView name, address;
        if (convertView == null) {
            //having problems with the convertVeiw when not null, so just redoing it each time.  ...
            LayoutInflater inflator = context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.row, null);
        }

        name = convertView.findViewById(R.id.name);

        name.setText("Name: " + list.get(position).getDevice().getName());
        address = convertView.findViewById(R.id.address);
        address.setText("Address: " + list.get(position).getDevice().getAddress());
        String data="";
        String decryptedData="";
        bytes=list.get(position).getScanRecord().getBytes();
        String byteInfo="Byte Size:"+bytes.length;
        int count = 0;
        for(Byte b:bytes)
        {
            count++;
            byteInfo=byteInfo+"byte"+count+":"+offset(b)+" ";
        }
         if(bytes.length>30)
         {
             data=data+"Hex ProductName:"+offset(bytes[8])+offset(bytes[7])+offset(bytes[6])+offset(bytes[5])+" ";
             data=data+"Hex SerialNumber:"+offset(bytes[12])+offset(bytes[11])+offset(bytes[10])+offset(bytes[9])+" ";
             data=data+"Hex Distance:"+offset(bytes[14])+offset(bytes[13])+" ";
             data=data+"Hex Level:"+offset(bytes[16])+offset(bytes[15])+"\n";

             Log.v("Prod name",offset(bytes[8])+offset(bytes[7])+offset(bytes[6])+offset(bytes[5])+" ");
             //Log.v("Serial nmber",Integer.parseInt((offset(bytes[12])+offset(bytes[11])+offset(bytes[10])+offset(bytes[9])),16)+" ");
             data=data+"Decrypted ProductName:"+addFirstByte(offset(bytes[8]))+addFirstByte(offset(bytes[7]))+addFirstByte(offset((bytes[6])))+addFirstByte(offset(bytes[5]))+" ";
             data=data+"Decrypted SerialNumber:"+offset(bytes[12])+offset(bytes[11])+offset((bytes[10]))+offset(bytes[9])+" ";
             data=data+"Decrypted Distance:"+addFirstByte(offset(bytes[14]))+addFirstByte(offset(bytes[13]))+" ";
             data=data+"Decrypted Level:"+addFirstByte(offset(bytes[16]))+addFirstByte(offset(bytes[15]))+" ";



             data=data+"ProductName:"+Long.parseLong(addFirstByte(offset(bytes[8]))+addFirstByte(offset(bytes[7]))+addFirstByte(offset((bytes[6])))+addFirstByte(offset(bytes[5])),16)+" ";
             data=data+"SerialNumber:"+Long.parseLong((offset(bytes[12])+offset(bytes[11])+offset((bytes[10]))+offset(bytes[9])),16)+" ";
             data=data+"Distance:"+Long.parseLong(addFirstByte(offset(bytes[14]))+addFirstByte(offset(bytes[13])),16)+" ";
             data=data+"Level:"+Long.parseLong(addFirstByte(offset(bytes[16]))+addFirstByte(offset(bytes[15])),16)+" ";


             if((list.get(position).getDevice().getName()!=null)&&(list.get(position).getDevice().getName().contains("Dusun")))
             {
                  data=data+"Liquid Sensor Check 1:"+offset(bytes[22])+offset(bytes[21])+" ";
                 data=data+"Liquid Sensor Check 2:"+offset(bytes[20])+offset(bytes[19])+" ";
                 data=data+"Liquid Sensor Check 3:"+offset(bytes[18])+offset(bytes[17])+" ";
                 data=data+"Liquid Sensor Check 4:"+offset(bytes[16])+offset(bytes[15])+"\n";

                 data=data+"Liquid Sensor Check 1:"+Long.parseLong(offset(bytes[22])+offset(bytes[21]),16)+" ";
                 data=data+"Liquid Sensor Check 2:"+Long.parseLong(offset(bytes[20])+offset(bytes[19]),16)+" ";
                 data=data+"Liquid Sensor Check 3:"+Long.parseLong(offset(bytes[18])+offset(bytes[17]),16)+" ";
                 data=data+"Liquid Sensor Check 4:"+Long.parseLong(offset(bytes[16])+offset(bytes[15]),16)+" ";
             }

             decryptedData=decryptedData+offset(bytes[0])+offset(bytes[1])+offset(bytes[2])+offset(bytes[3]);
             decryptedData=decryptedData+offset(bytes[4])+offset(bytes[5])+offset(bytes[6])+offset(bytes[7]);
             decryptedData=decryptedData+offset(bytes[8])+offset(bytes[9]);
             decryptedData=decryptedData+offset(bytes[10])+offset(bytes[11]);
             decryptedData=decryptedData+offset(bytes[12])+offset(bytes[13]);
             decryptedData=decryptedData+offset(bytes[14])+offset(bytes[15]);
             decryptedData=decryptedData+offset(bytes[16]);
             decryptedData=decryptedData+offset(bytes[17]);
             decryptedData=decryptedData+offset(bytes[18]);
             decryptedData=decryptedData+offset(bytes[19]);
         }

        TextView dataField = convertView.findViewById(R.id.data);
        dataField.setText("byteInfo: " + byteInfo);

        TextView dexcryptedDataField = convertView.findViewById(R.id.decryptedData);
        dexcryptedDataField.setText("Hex Data: " + data);

        return convertView;
    }

    public String offset(byte encryptedData)
    {
        int decimal = (int) encryptedData & 0xff;               // bytes widen to int, need mask, prevent sign extension
        // get last 8 bits
        String hex = Integer.toHexString(decimal);
        if (hex.length() % 2 == 1) {                    // if half hex, pad with zero, e.g \t
            hex = "0" + hex;
        }
        hex.replaceFirst("^0+(?!$)", "");
        return hex;
    }

    public String addFirstByte(String encryptedData)
    {
        Long serialNumber=Long.parseLong("54",16);
        if(offset(bytes[12]).equals("54"))
        {
            logs=logs+"Start:";
        }
        Long encData=Long.parseLong(encryptedData,16);
        if((encData>serialNumber)||(encData.equals(serialNumber))) {

            encData = encData - serialNumber;
            if(offset(bytes[12]).equals("54"))
            {
                logs=logs+"First If Data:"+encData+" Sno:"+serialNumber;
            }
        }
        else
        {

            encData = encData + Long.parseLong("100",16);

            encData = encData - serialNumber;

            if(offset(bytes[12]).equals("54"))
            {
                logs=logs+"Second If Data:"+encData+" Sno:"+serialNumber;
            }

        }

        //long decimal =  encData & 0xff;               // bytes widen to int, need mask, prevent sign extension
        // get last 8 bits
        String hex = Long.toHexString(encData);
        if(offset(bytes[12]).equals("54"))
        {
            logs=logs+"Hex Value:"+hex;
        }
        if (hex.length() % 2 == 1) {                    // if half hex, pad with zero, e.g \t
            hex = "0" + hex;
        }
        hex.replaceFirst("^0+(?!$)", "");
        return hex;


    }
}
